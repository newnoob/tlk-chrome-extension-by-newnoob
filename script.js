const bannedScenarios = [
    'Keep an eye on them',
    'Look for them',
    'Listen',
];

const click = target => {
    if (target) {
        target.click();
    }
};

const delayedChain = (chain, delay) => {
    if (chain.length > 0) {
        chain[0]();
        setTimeout(delayedChain, delay, chain.slice(1, chain.length), delay);
    }
};

const train = () => {
    delayedChain([
        () => { click(document.querySelector('.button.train-button:first-child')); },
        () => { click(document.querySelector('.button.train-button:first-child')); },
        () => { click(document.querySelector('.button.stay')); },
    ], 100);
    setTimeout(() => {
        const healButton = document.querySelector('.HeroData .Table thead tr td.Center button');
        if (healButton && healButton.classList.value.indexOf('ng-hide') == -1) {
            return;
            heal();
        }
        setTimeout(() => {
            click(document.querySelector('.button.stay'));
        }, 100);
    }, 1200 + Math.random() * 300);
};

const heal = () => {
    if (healButton.disabled) {
        alert('cannot FH');
        return;
    }
    console.log('FHed');
    click(document.querySelector('.HeroData .Table thead tr td.Center button'));
};

const skip = () => {
    click(document.querySelector('.button.train-button:last-child'));
};

// hides train button for banned scenarios
const updateStyle = target => {
    if (bannedScenarios.indexOf(target.textContent) >= 0) {
        target.style.display = 'none';
    } else {
        target.style.display = 'block';
    }
};

// automatically trains if good scenario encountered
const autoTrain = target => {
    if (bannedScenarios.indexOf(target.textContent) >= 0) {
        skip();
    } else {
        train();
    }
};

// force enables heal button
const forceEnableHealButton = () => {
    const healButton = document.querySelector('.HeroData .Table thead tr td.Center button');
    if (healButton && healButton.disabled) {
        healButton.removeAttribute('disabled');
    }
};

const observeMutation = () => {
    // forceEnableHealButton();
    setTimeout(() => {
        const trainButton = document.getElementsByClassName('button train-button')[0];
        if (trainButton && !trainButton.disabled) {
            autoTrain(trainButton);
        }
    }, Math.random() * 500);
};

setInterval(observeMutation, 500);
